libauthen-bitcard-perl (0.90-3) UNRELEASED; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:35:08 +0100

libauthen-bitcard-perl (0.90-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 11:42:04 +0100

libauthen-bitcard-perl (0.90-2) unstable; urgency=medium

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove AGOSTINI Yves from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Convert d/copyright to copyright-format 1.0
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4

 -- Florian Schlichting <fsfs@debian.org>  Thu, 21 Jun 2018 22:45:15 +0200

libauthen-bitcard-perl (0.90-1) unstable; urgency=low

  * New upstream release.
  * Drop patch spelling.patch (applied upstream).
  * Add (build-)dep on liblwp-protocol-https-perl.
  * debian/copyright: Update for new release; refer to "Debian systems"
    instead of "Debian GNU/Linux systems".
  * Bump Standards-Version to 3.9.2.
  * Update my email address.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 12 Jun 2011 15:02:42 +0200

libauthen-bitcard-perl (0.89-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release (0.87).
  * Standards-Version 3.8.3 (drop perl version dependency)
  * Add myself to Uploaders

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * New upstream release (0.89).
    + Now licensed under Apache-2.0.
  * debian/copyright: Update for new upstream release, formatting changes for
    current DEP-5 proposal.
  * Fix spelling error in documentation.
    + new patch: spelling.patch
  * Remove (build-)dep on libdigest-sha1-perl.
  * Add (build-)dep on libjson-perl.
  * Add build-dep on libtest-script-perl.
  * Use source format 3.0 (quilt).
  * Bump Standards-Version to 3.9.1.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Tue, 03 Aug 2010 12:27:25 +0900

libauthen-bitcard-perl (0.86-1) unstable; urgency=low

  * Initial Release. (Closes: #490932)

 -- AGOSTINI Yves <agostini@univ-metz.fr>  Tue, 15 Jul 2008 14:19:39 +0200
